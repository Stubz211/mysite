# coding=utf-8
from django.conf.urls import url
from . import views

urlpatterns = [
    # URL() - Принимает 4 аргумента, обязательные - regex и view
    # Не обязательные - kwargs, name
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'),
    url(r'^(?P<pk>[0-9]+)/results/$', views.ResultsView.as_view(), name='results'),
    url(r'^(?P<question_id>[0-9]+)/vote/$', views.vote, name='vote'),
]
