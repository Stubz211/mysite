from django.shortcuts import render
from django.template import loader
import datetime
# Create your views here.


def current_time(request):
    cur_time = datetime.datetime.today()
    time = ("{0} : {1} : {2}".format(cur_time.hour + 5, cur_time.minute, cur_time.second))
    return render(request, 'index/index.html', {'key_time': time})
